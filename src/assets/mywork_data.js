import project1_img from '../assets/project1_img.png'
import project2_img from '../assets/project_2.svg'
import project3_img from '../assets/project_3.svg'
import project4_img from '../assets/project_4.svg'
import project5_img from '../assets/project_5.svg'
import project6_img from '../assets/project_6.svg'

const mywork_data = [
    {
        w_no:1,
        w_name:"Web design",
        w_img:project1_img,
        w_href: 'https://guess-my-number-prem4120715-8ce23158e0e9982c9e011438910530669a7.gitlab.io/'
    },
    {
        w_no:2,
        w_name:"Web design",
        w_img:project2_img
    },
    {
        w_no:3,
        w_name:"Web design",
        w_img:project3_img
    },
    {
        w_no:4,
        w_name:"Web design",
        w_img:project4_img
    },
    {
        w_no:5,
        w_name:"Web design",
        w_img:project5_img
    },
    {
        w_no:6,
        w_name:"Web design",
        w_img:project6_img
    },
]
 
export default mywork_data;