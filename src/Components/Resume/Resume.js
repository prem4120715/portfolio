import React from 'react'
import './Resume.css'

const Resume = () => {
  return (
    <div id='resume' className='resume'>
        <header>
            <h1>Prem Khidbide</h1>
            <p>Frontend Developer</p>
        </header>
        <section className='contact-info'>
            <h2>Contact Informartion</h2>
            <p>Email: premkhidbide11@gmail.com</p>
            <p>Phone: +91 9987570059</p>
            <p>Location: Mumbai, Maharashtra</p>
        </section>
        <section className='education'>
            <h2>Education</h2>
            <p>Bachelor of Information Technology</p>
            <p>University of Mumbai, 2019-2022</p>
        </section>
        <section className='experience'>
            <h2>Experience</h2>
            <p>Frontend Developer at ABC Company</p>
            <p>2019-Present</p>
            <ul>
          <li>Developed responsive web applications using React.js.</li>
          <li>Collaborated with design and backend teams to deliver projects.</li>
          <li>Implemented UI/UX designs to create seamless user experiences.</li>
        </ul>
        </section>
    </div>
  )
}

export default Resume