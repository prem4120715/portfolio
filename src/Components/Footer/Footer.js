/* eslint-disable no-unused-vars */
import React from 'react'
import './Footer.css';
import { FaGithubSquare, FaTwitter, FaLinkedin,FaInstagram  } from "react-icons/fa";


const Footer = () => {
    return (
<footer class="footer">
    <hr />
    <br/>
    <div class="footer-container">
        <div class="footer-about">
            <h2>About Me</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.</p>
        </div>
        <div class="footer-links">
            <h2>Quick Links</h2>
            <ul>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#work">Portfolio</a></li>
                <li><a href="#contact">Contact</a></li>
                
            </ul>
        </div>
        <div class="footer-social">
            <h2>Follow Me</h2>
            <ul>
                {/* <li><FaTwitter />  <a href="https://twitter.com" target="_blank" rel='noreferrer'>Twitter</a></li> */}
                <li><FaLinkedin />  <a href="https://linkedin.com" target="_blank" rel='noreferrer'>LinkedIn</a></li>
                <li><FaGithubSquare/> <a href="https://github.com" target="_blank" rel='noreferrer'>GitHub</a></li>
                <li><FaInstagram />  <a href="https://instagram.com" target="_blank" rel='noreferrer'>Instagram</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-bottom">
        <p>&copy; 2024 Prem Khidbide. All rights reserved.</p>
    </div>
</footer>

    )
}

export default Footer