import React from 'react';
import './Hero.css';
// import profile_img from '../../assets/profile_img.svg';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import profile from '../../assets/profile.jpg'

const Hero = () => {
  return (
    <div id='home' className='hero'>
      {/* <img src={profile_img} alt='' /> */}
      <img src={profile} alt='' />
      <h1><span>I'm Prem Khidbide</span>, Full Stack Developer based in Mumbai</h1>
      <p>I am a Full Stack Developer from Mumbai, India with 2 years of experience in company like Servion Global Solutions Private Ltd.</p>
      <div className='hero-action'>
        <div className='hero-connect'><AnchorLink className='anchor-link' offset={50} href='#contact'>Connect With Me</AnchorLink></div>
        <div className='hero-connect2'><a href="https://drive.google.com/file/d/16wHLExmtsTUL8Tk5xQeZmNTb2mBZfTDd/view?usp=sharing"  rel='noreferrer' target="_blank" >Resume</a>
      </div>
      </div>
    </div>
  )
}


export default Hero;